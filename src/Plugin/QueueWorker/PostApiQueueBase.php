<?php

namespace Drupal\post_api\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Site\Settings;
use Drupal\post_api\Event\QueueItemProcessedEvent;
use Drupal\post_api\Event\QueueProcessingCompleteEvent;
use Drupal\post_api\Service\Request;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the Post APi Queue Workers.
 */
abstract class PostApiQueueBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The default QueueWorker runtime in seconds.
   */
  const DEFAULT_QUEUEWORKER_RUNTIME = 60;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * Request.
   *
   * @var \Drupal\post_api\Service\Request
   */
  protected $request;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Event Dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Creates a new PostApiBase object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   Queue Factory.
   * @param \Drupal\post_api\Service\Request $request
   *   Request.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A date time instance.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   Event Dispatcher.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Request.
   */
  public function __construct(QueueFactory $queue, Request $request, ConfigFactoryInterface $config_factory, TimeInterface $time, ContainerAwareEventDispatcher $eventDispatcher, LoggerChannelFactoryInterface $logger) {
    $this->queue = $queue;
    $this->request = $request;
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('queue'),
      $container->get('post_api.request'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('event_dispatcher'),
      $container->get('logger.factory')
    );
  }

  /**
   * Processes items in the queue.
   */
  public function processQueue() {
    // Call content push queue service, and create an instance for processing.
    $queue = $this->queue->get('post_api_queue');

    // Get the number of items.
    $number_in_queue = $queue->numberOfItems();
    $number_processed = 0;

    $config = $this->configFactory->get('post_api.settings');
    $api_rate_limit = $config->get('api_rate_limit_per_minute') ?: 0;
    $max_runtime = $config->get('queueworker_max_runtime') ? $config->get('queueworker_max_runtime') : static::DEFAULT_QUEUEWORKER_RUNTIME;
    $finish_time = $this->time->getCurrentTime() + $max_runtime;

    for ($i = 0; $i < $number_in_queue; $i++) {
      if ($this->time->getCurrentTime() >= $finish_time) {
        // If the QueueWorker has exceeded max runtime - stop processing.
        break;
      }

      $item_process_start_time = microtime(TRUE);
      $item = $queue->claimItem($max_runtime);
      if (!empty($item)) {
        // Now process individual item.
        $response = $this->processItem($item->data);
        $response_code = ($response instanceof ResponseInterface) ? $response->getStatusCode() : 500;
        $reason_phrase = ($response instanceof ResponseInterface) ? $response->getReasonPhrase() : 'none provided';
        // Dispatch an event to which Event Subscribers can react.
        // E.g. need special logging for various 20* responses.
        $event_item_processed = new QueueItemProcessedEvent($response, $item->data);
        $this->eventDispatcher->dispatch($event_item_processed, 'post_api_queue_item_processed_event');
        if (in_array($response_code, [200, 201, 202])) {
          // API POST is a success - delete processed item from queue.
          $queue->deleteItem($item);
          // Update count of success within current QueueWorker run.
          $number_processed = $number_processed + 1;
        }
        else {
          // Item not removed from Queue. Make minor noise.
          // More specific logging can be handled in local event subscriber.
          $message = sprintf('Post API: Response code %d prevented removal of post_api_queue item %d from queue. Reason: %s', $response_code, $item->item_id, $reason_phrase);
          $this->logger->get('post_api')->error($message);
        }
      }

      $this->controlRateLimit($api_rate_limit, $item_process_start_time);
    }

    $number_released = $number_in_queue - $number_processed;
    if ($number_in_queue > 0) {
      // Log only when queue is not empty.
      if ($number_in_queue === $number_released) {
        // If there's no change between input and output and the queue
        // was not empty, then log as an error.
        $this->logError($number_in_queue);
      }
      else {
        // Log number of items processed.
        $this->logInfo($number_in_queue, $number_processed);
      }
    }

    // Dispatch an event to which Event Subscribers can react.
    $event_queue_complete = new QueueProcessingCompleteEvent($number_in_queue, $number_released);
    $this->eventDispatcher->dispatch($event_queue_complete, 'post_api_queue_processing_complete_event');
  }

  /**
   * Processes a single queued item.
   *
   * @param mixed $data
   *   The data that was added to the queue.
   *
   * @return \Psr\Http\Message\ResponseInterface|int|bool
   *   The response or errors from the request.
   */
  public function processItem($data) {
    $apikey = Settings::get('post_api_apikey', NULL);
    $endpoint_path = $data['endpoint_path'] ?? NULL;
    $endpoint = Settings::get('post_api_endpoint_host', NULL) . $endpoint_path;
    $payload = $data['payload'] ?? [];

    // Send POST request and return the response.
    return $this->request->sendRequest($endpoint, $apikey, $payload);
  }

  /**
   * Logs queue processing error.
   *
   * @param int $queue_total
   *   Total number of items in queue.
   */
  protected function logError(int $queue_total) {
    $message = sprintf('Post API: failure to process queued items. Total items in queue: %d.', $queue_total);
    $this->logger->get('post_api')->error($message);
  }

  /**
   * Logs summary of processed queue items.
   *
   * @param int $queue_total
   *   Total number of items in queue at the beginning of the queue processing.
   * @param int $queue_processed
   *   Number of processed items.
   */
  protected function logInfo(int $queue_total, int $queue_processed) {
    $message = sprintf('Post API: processed %d out of %d queued items.',
      $queue_processed, $queue_total);
    $this->logger->get('post_api')->info($message);
  }

  /**
   * Limit the rate of speed for API requests.
   *
   * Rate of processing queue items will be controlled only if api rate limit
   * is set in Post API settings.
   *
   * API rate limiting is implemented using the slow-down strategy, which evenly
   * spreads queue items execution throughout 1 minute time-frame, rather then
   * processing allowed amount as fast as possible and waiting for the remainder
   * time to pass. This approach will cover requests to APIs that enforce rate
   * limits based on speed or quantity.
   *
   * @param int $api_rate_limit
   *   API rate limit per minute.
   * @param float $item_process_start_time
   *   Unix timestamp with microseconds.
   *
   * @todo Allow API rate limit customization based on more granular time
   * intervals. E.g. requests per second.
   */
  protected function controlRateLimit(int $api_rate_limit, $item_process_start_time) {
    if ($api_rate_limit <= 0) {
      return;
    }

    // Calculate max time that can be consumed to process one item according to
    // set api rate limit in seconds.
    $seconds_per_item = 60 / $api_rate_limit;

    // Calculate time left for processing current item in microseconds.
    $microseconds_left = ($seconds_per_item - (microtime(TRUE) - $item_process_start_time)) * 1000000;
    if ($microseconds_left > 0) {
      // Item is processed faster than the time allotted by rate limit.
      // Wait until allotted time have passed before processing next item.
      usleep($microseconds_left);
    }
  }

}
