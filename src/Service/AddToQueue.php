<?php

namespace Drupal\post_api\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for adding an item to the queue.
 */
class AddToQueue {



  /**
   * The config settings for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * A unique id for the queue item to use in deduping the queue.
   *
   * If dedupe flag set to TRUE while the item is being added to the queue,
   * any other queue items with this uid will be removed.
   *
   * @var string
   */
  private $queueItemUid = NULL;

  /**
   * Dedupe flag.
   *
   * Should be set during new item addition to the queue.
   * This flag defines whether existing queue items with
   * the same unique id (uid) should be released from queue.
   *
   * @var bool
   */
  private $flagDedupe = FALSE;

  /**
   * Queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private $queue;

  /**
   * AddToQueue constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   Queue factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(QueueFactory $queue, ConfigFactoryInterface $config_factory) {
    $this->queue = $queue;
    $this->config = $config_factory->get('post_api.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('queue'),
      $container->get('config.factory')
    );
  }

  /**
   * Returns queue object.
   */
  private function getQueue() {
    return $this->queue->get('post_api_queue');
  }

  /**
   * Builds queue item data.
   *
   * @param array $item_data
   *   Associative array where key is a property name and value is data
   *   associated with that property.
   *
   * @return object
   *   Structured queue item data.
   */
  public function buildQueueItemData(array $item_data) {
    $data = new \stdClass();
    // @todo Display warning if uid is not set for the item.
    foreach ($item_data as $key => $value) {
      $data->{$key} = $value;
    }

    return $data;
  }

  /**
   * Adds the entity the to the Drupal Queue.
   */
  public function addToQueue($data, $flag_dedupe = FALSE) {
    $queueing_enabled = !$this->config->get('disable_queueing');
    if (!empty($data) && $queueing_enabled) {
      // Add item to queue.
      $queue = $this->getQueue();
      $item = $this->buildQueueItemData($data);
      $this->flagDedupe = $flag_dedupe;
      $this->queueItemUid = $item->uid ?? NULL;
      $this->dedupeQueue($queue);
      $queue->createItem($data);
    }
  }

  /**
   * Remove any queued items with the same $queue_uid as this one.
   */
  private function dedupeQueue($queue) {
    if ($this->flagDedupe === TRUE) {
      $item_count = $queue->numberOfItems();
      $removed_count = 0;
      // The queue will keep grabbing the same item after it is released, so
      // we need to grab them all and mass release them.
      $queued_items_to_release = [];
      for ($i = 0; $i < $item_count; $i++) {
        // Claim queued item.
        $queued_item = $queue->claimItem();
        if ($queued_item && $this->queueItemUid) {
          try {
            // Process the item - check if it has the same unique id as the
            // one we will be adding.
            if ($this->queueItemUid === $queued_item->data['uid']) {
              // Item uid matches the new one - delete it.
              $queue->deleteItem($queued_item);
              $removed_count++;
            }
            else {
              // It is not a duplicate - release it into the queue.
              $queued_items_to_release[$queued_item->item_id] = $queued_item;
            }
          }
          catch (SuspendQueueException $e) {
            // In case of Exception - release the item that the worker could
            // not process. It will be processes in the next batch.
            $queued_items_to_release[$queued_item->item_id] = $queued_item;
            continue;
          }
        }
      }

      // Release the non-duplicates.
      foreach ($queued_items_to_release as $queued_items_to_release) {
        $queue->releaseItem($queued_items_to_release);
      }
    }
  }

}
