<?php

namespace Drupal\post_api\Plugin\QueueWorker;

/**
 * Queue worker: POSTs queued items' payloads to provided endpoint on CRON run.
 *
 * Cron run will process as many many item as it can within 60 seconds.
 *
 * @QueueWorker(
 *   id = "post_api_queue",
 *   title = @Translation("Post API Queue")
 * )
 */
class PostApiQueueWorker extends PostApiQueueBase {}
