<?php

namespace Drupal\post_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Builds the PostApiSettings Form.
 */
class PostApiSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'post_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('post_api.settings');

    // Variables to compose Post API status.
    $set = '<span style="color:green">&#x2714;</span>';
    $error = '<span style="color:red">&#x2716;</span>';
    $post_api_endpoint_host = Settings::get('post_api_endpoint_host', NULL);
    $post_api_apikey = Settings::get('post_api_apikey', NULL);
    $post_api_endpoint_host_indicator = $post_api_endpoint_host ? $set : $error;
    $post_api_apikey_indicator = $post_api_apikey ? $set : $error;
    $post_api_endpoint_host_markup = $post_api_endpoint_host ? $post_api_endpoint_host : $this->t('Endpoint host is not set in settings.php. Review <a href="@readme">README.md</a> for configuration instructions.', ['@readme' => Url::fromUri('internal:/admin/help/post_api')->toString()]);
    $post_api_apikey_markup = $post_api_apikey ? $this->t('Set.') : $this->t('Apikey is not set in settings.php. Review <a href="@readme">README.md</a> for configuration instructions.', ['@readme' => Url::fromUri('internal:/admin/help/post_api')->toString()]);

    $form['status'] = [
      '#type' => 'details',
      '#title' => $this->t('Post API status'),
      '#open' => TRUE,
    ];

    $form['status']['endpoint_host'] = [
      '#type' => 'markup',
      '#markup' => Markup::create('<p>' . $post_api_endpoint_host_indicator . ' <strong>' . $this->t('Endpoint Host:') . '</strong> ' . $post_api_endpoint_host_markup . '</p>'),
    ];

    $form['status']['apikey'] = [
      '#type' => 'markup',
      '#markup' => Markup::create('<p>' . $post_api_apikey_indicator . ' <strong>' . $this->t('Apikey:') . '</strong> ' . $post_api_apikey_markup . '</p>'),
    ];

    $form['config'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration options'),
      '#open' => TRUE,
    ];

    $form['config']['api_rate_limit_per_minute'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('API rate limit per minute'),
      '#description' => $this->t('A positive integer that specifies the API rate limit per minute. If set, this value will be applied to all endpoints that are defined for each queue item. If set to "0", rate limiting will not be applied.'),
      '#default_value' => $config->get('api_rate_limit_per_minute'),
      '#field_suffix' => $this->t('requests per minute'),
    ];

    $form['config']['divider_1'] = [
      '#type' => 'markup',
      '#markup' => '<hr>',
    ];

    $form['config']['pause_cron_processing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause processing on Cron'),
      '#description' => $this->t('If checked, the queue will not be processed during Cron runs. Manual queue processing is still available.'),
      '#default_value' => $config->get('pause_cron_processing'),
    ];

    $form['config']['disable_queueing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable adding items to the queue.'),
      '#description' => $this->t('If checked, items that would normally add to the queue, do not add to the queue.'),
      '#default_value' => $config->get('disable_queueing'),
    ];

    $form['config']['divider_2'] = [
      '#type' => 'markup',
      '#markup' => '<hr>',
    ];

    $form['config']['queueworker_max_runtime'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => ini_get('max_execution_time'),
      '#title' => $this->t('QueueWorker max runtime (sec)'),
      '#description' => $this->t('QueueWorker max runtime <em>in seconds</em> defines the maximum amount of time that Post API QueueWorker will allocate to processing the queue.<br/>If there are more queue items than the QueueWorker can process in one run, they will remain in queue until QueueWorker runs next.<br><strong>Must be a positive integer that is less than PHP max_execution_time value set for the environment.</strong>'),
      '#default_value' => $config->get('queueworker_max_runtime'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => '20',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = self::configFactory()->getEditable('post_api.settings');
    $config
      ->set('queueworker_max_runtime', $form_state->getValue('queueworker_max_runtime'))
      ->set('api_rate_limit_per_minute', $form_state->getValue('api_rate_limit_per_minute'))
      ->set('pause_cron_processing', $form_state->getValue('pause_cron_processing'))
      ->set('disable_queueing', $form_state->getValue('disable_queueing'))
      ->save();
    $this->messenger()->addStatus('Post API settings have been updated.');
  }

}
