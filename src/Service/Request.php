<?php

namespace Drupal\post_api\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send request to endpoint.
 */
class Request implements ContainerFactoryPluginInterface {

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Constructs a Request object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger) {
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static();
  }

  /**
   * Send request to endpoint.
   *
   * @param string $endpoint
   *   Full endpoint url.
   * @param string $apikey
   *   Apikey.
   * @param array $payload
   *   An array of data to be sent in request body.
   *
   * @return \Psr\Http\Message\ResponseInterface|int|bool
   *   Response object OR FALSE for exceptions.
   *
   * @throws \GuzzleHttp\Exception\ServerException
   * @throws \GuzzleHttp\Exception\RequestException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendRequest($endpoint, $apikey, array $payload) {
    $headers = [
      'apikey' => $apikey,
      'Content-Type' => 'application/json',
    ];

    $body = Json::encode($payload);

    try {
      $response = $this->httpClient->request('POST', $endpoint, [
        'headers' => $headers,
        'body' => $body,
      ]);
      return $response;
    }
    catch (ServerException $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('post_api'), $e, $e->getCode() . ' ' . $e->getMessage()), fn() => watchdog_exception('post_api', $e, $e->getCode() . ' ' . $e->getMessage()));
      return $e->getCode();
    }
    catch (RequestException $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('post_api'), $e, $e->getCode() . ' ' . $e->getMessage()), fn() => watchdog_exception('post_api', $e, $e->getCode() . ' ' . $e->getMessage()));
      return $e->getCode();
    }
    catch (\Exception $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('post_api'), $e), fn() => watchdog_exception('post_api', $e));
      return FALSE;
    }
  }

}
