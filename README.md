# POST API

## INTRODUCTION
Post API is a module written for developers that would like to utilize Drupal
Queue API for POSTing to specified endpoint(s).

### POST API QUEUE UI

This module provides the most basic Queue management UI from which you can
review and manually process the queue. Visit /admin/config/post-api/queue
or navigate to (Admin > Configuration > Web Services > Post API > Queue).

### DISPATCHED EVENTS

Post API module dispatches two events. 

**1. post_api_queue_item_processed_event**

Event is dispatched once single queue item is processed by QueueWorker.
The Event provides the following data to Event Subscribers:
- API request response code
- Queue item data

**2. post_api_queue_processing_complete_event**

Event is dispatched once the Post API queue processing is complete. The Event
provides the following data to Event Subscribers:
 
- Number of queue items at the beginning of queue processing
- Number of queue items released back to the queue (unprocessed/errored)
at the end of queue processing

An example of a custom Event Subscriber use case could be: "A Slack notification
should be sent if the Post API queue worker did not process any items during
queue execution." Your use case _will_ differ.

## REQUIREMENTS

This module has no dependencies.

NOTE: Core has a queue bug, where claimItem() method in the database and memory
queues does not use expire lease timestamps correctly. Since this module uses
DatabaseQueue, its proper function depends on the following core patch being
applied - https://www.drupal.org/project/drupal/issues/2893933#comment-13413895

## INSTALLATION

Install post_api module via Composer.

`composer require drupal/post_api`

Example use case: As an engineer I need to post content of a new or updated node
to an endpoint, using API key authentication, and I want to use Queue API, so
that I can trigger my content sync on Cron runs.

### STEP 1

Enable Post API module.

### STEP 2

In the custom module, add hook_entity_insert() and hook_entity_update() that
will call a custom helper function that uses queue service from Post API to
queue desired info for later processing.

Example code:

```php
/**
 * Implements hook_entity_update().
 *
 * Queues updated items for sync.
 */
function post_api_entity_update(EntityInterface $entity) {
  _post_api_add_entity_to_queue($entity);
}

function _post_api_add_facility_to_queue(EntityInterface $entity) {
  // Let's assume we need to queue only nodes of type example_bundle_ and
  // example_bundle_2.
  $entity_bundles = [
    'example_bundle_1',
    'example_budnle_2',
  ];

  if ($entity->getEntityTypeId() === 'node'
    && in_array($entity->bundle(), $entity_bundles)) {
    // Load Post API Queue service.
    $queue = Drupal::service('post_api.add_to_queue');

    // Form queue item data.
    // (Optional) Queue item's Unique ID. This is needed if your use case
    // requires removing duplicate items from queue.
    $data['uid'] = 'GENERATE_UNIQUE_ID_BASED_ON_ENTITY_PROPS';

    // All $data properties listed below are required for successful queue item
    //processing.
    // NOTE: endpoint_path should only contain endpoint path, not the host.
    $data['endpoint_path'] = 'your-endpoint-path';

    // Set payload. Default payload provided by this module is empty. You
    // must set your own payload.
    // Entity fields (updated and original) can be compared and processed in
    // order to structure the payload array.
    $data['payload'] = _post_api_get_custom_payload($entity);

    // Only add to queue if payload is not empty.
    // If its empty, it means that there is no new information to send
    // to the endpoint.
    if (!empty($data['payload'])) {
    // Second parameter is a value of $flagDedupe,
    // which defines whether or not pre-existing queue items
    // with same unique id should be removed from queue before
    // before adding current item.
      $queue->addToQueue($data, TRUE);
    }
  }
}
```

Check how [VA.gov](https://github.com/department-of-veterans-affairs/va.gov-cms)
is using this module.

### STEP 3

Set `post_api_endpoint_host` and `post_api_apikey` variables in settings.php.
Once variables are set and cache is cleared, you may check if values are
recognized by Post API at `/admin/config/post-api/config`.

```php
$settings['post_api_endpoint_host'] = getenv('API_URL') ?: FALSE;
$settings['post_api_apikey'] = getenv('API_KEY') ?: FALSE;
```

### STEP 4

Configure Cron to run at desired intervals.

## CONFIGURATION

See available settings at `/admin/config/post-api/config`.

Settings include:
- API rate limit per minute
- Pause processing on Cron
- QueueWorker max runtime (sec)
