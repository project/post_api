<?php

namespace Drupal\Tests\post_api\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\Core\Queue\QueueInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests Post API queue.
 *
 * Queues and dequeues a set of items to check the basic queue functionality.
 * Tests queue item expiration.
 * Tests queue dedupe function.
 * NOTE: API requests are not included in test coverage, since those would be
 * specific to the project using this module.
 *
 * @group PostApiQueue
 */
class PostApiQueueTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['post_api'];

  /**
   * Mock queue item data.
   *
   * @var array
   */
  public static $mockData = [
    'nid' => 1234,
    'uid' => 'unique_queue_item_id',
    'endpoint_path' => '/endpoint_path',
    'payload' => [
      'status' => [
        'code' => 'TEST_CODE',
        'info' => 'Test info',
      ],
    ],
  ];

  /**
   * Tests the Post API queue.
   */
  public function testPostApiQueue() {
    // Create queue.
    $queue = new DatabaseQueue('post_api_queue', Database::getConnection());
    $queue->createQueue();
    $this->runBasicQueueTest($queue);
    $queue->deleteQueue();
  }

  /**
   * Tests queue item expiration.
   *
   * NOTE: Core has a bug, where claimItem in the database and memory queues
   * does not use expire correctly.
   * Expiration test depends on applied core patch
   * https://www.drupal.org/project/drupal/issues/2893933#comment-13413895
   * If patch is not applied, this test will fail.
   */
  public function testPostApiQueueItemExpiration() {
    $queue = $this->createQueue();

    // Test that we can claim an item that is expired and we cannot claim an
    // item that has not expired yet.
    $queue->createItem([$this->randomMachineName() => $this->randomMachineName()]);
    $item = $queue->claimItem();
    $this->assertNotFalse($item, 'The item can be claimed.');
    $item = $queue->claimItem();
    $this->assertFalse($item, 'The item cannot be claimed again.');
    // Set the expiration date to the current time minus the lease time plus 1
    // second. It should be possible to reclaim the item.
    $this->setExpiration($queue, time() - 31);
    $item = $queue->claimItem();
    // IMPORTANT: Since this assertion will surely fail until core patch is
    // merged, we'll test for expected failure here - means our expectation
    // is reversed.
    // Once this assertion starts failing on dev branch, it is an indicator
    // that the core patch is merged, and this test must be refactored to use
    // assertNotFalse().
    $this->assertFalse($item, 'Item can be claimed after expiration.');

    $queue->deleteQueue();
  }

  /**
   * Queues items and verifies that dedupe method works as expected.
   */
  public function testPostApiQueueDedupe() {
    // Create queue.
    $queue = $this->createQueue();

    // Check that queue is empty.
    $this->assertSame(0, $queue->numberOfItems(), 'ERROR: Post API Queue is not empty');

    // Dedupe test.
    $add_to_queue_service = \Drupal::service('post_api.add_to_queue');
    // Dedupe while adding to queue.
    $add_to_queue_service->addToQueue(self::$mockData, TRUE);
    $add_to_queue_service->addToQueue(self::$mockData, TRUE);

    $this->assertSame(1, $queue->numberOfItems(), 'ERROR: Post API Queue contains duplicate items.');

    $queue->deleteQueue();
  }

  /**
   * Queues and dequeues a set of items to check the basic queue functionality.
   *
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   An instantiated queue object.
   */
  protected function runBasicQueueTest(QueueInterface $queue) {
    // Create four random items.
    $data = [];
    for ($i = 0; $i < 4; $i++) {
      $data[] = [$this->randomMachineName() => $this->randomMachineName()];
    }

    // Queue items 1 and 2 in the queue.
    $queue->createItem($data[0]);
    $queue->createItem($data[1]);

    // Retrieve two items from queue.
    $items = [];
    $new_items = [];

    $items[] = $item = $queue->claimItem();
    $new_items[] = $item->data;

    $items[] = $item = $queue->claimItem();
    $new_items[] = $item->data;

    // First two dequeued items should match the first two items we queued.
    $this->assertEquals($this->queueScore($data, $new_items), 2, 'Two items matched');

    // Add two more items.
    $queue->createItem($data[2]);
    $queue->createItem($data[3]);

    $this->assertSame(4, $queue->numberOfItems(), 'Post API Queue is empty after adding items.');

    $items[] = $item = $queue->claimItem();
    $new_items[] = $item->data;

    $items[] = $item = $queue->claimItem();
    $new_items[] = $item->data;

    // All dequeued items should match the items we queued exactly once,
    // therefore the score must be exactly 4.
    $this->assertEquals($this->queueScore($data, $new_items), 4, 'Four items matched');

    // There should be no duplicate items.
    $this->assertEquals($this->queueScore($new_items, $new_items), 4, 'Four items matched');

    $this->clearQueue($queue, $items);

    // Check that queue is empty.
    $this->assertSame(0, $queue->numberOfItems(), 'Post API Queue is empty');

    $queue->deleteQueue();
  }

  /**
   * Create Queue.
   *
   * @return \Drupal\core\Queue\DatabaseQueue
   *   Queue.
   */
  protected function createQueue() {
    // Create queue.
    $queue = new DatabaseQueue('post_api_queue', Database::getConnection());
    $queue->createQueue();
    return $queue;
  }

  /**
   * Set the expiration for different queues.
   *
   * @param object $queue
   *   The queue for which to alter the expiration.
   * @param int $expire
   *   The new expiration time.
   */
  protected function setExpiration($queue, $expire) {
    $class = get_class($queue);
    switch ($class) {
      case DatabaseQueue::class:
        \Drupal::database()
          ->update(DatabaseQueue::TABLE_NAME)
          ->fields(['expire' => $expire])
          ->execute();
        break;
    }
  }

  /**
   * Returns the number of equal items in two arrays.
   *
   * @param array $items
   *   Array of queue items.
   * @param array $new_items
   *   Array of queue items.
   *
   * @return int
   *   The number of equal items in two arrays
   */
  protected function queueScore(array $items = [], array $new_items = []) {
    $score = 0;
    foreach ($items as $item) {
      foreach ($new_items as $new_item) {
        if ($item === $new_item) {
          $score++;
        }
      }
    }
    return $score;
  }

  /**
   * Clear all items from queue.
   *
   * @param object $queue
   *   The queue for which items should be cleared.
   * @param array $items
   *   Array  of queue items.
   */
  protected function clearQueue($queue, array $items = []) {
    foreach ($items as $item) {
      $queue->deleteItem($item);
    }
  }

}
