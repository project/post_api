<?php

namespace Drupal\post_api\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the Queue processing finished event.
 */
class QueueProcessingCompleteEvent extends Event {

  const EVENT_NAME = 'post_api_queue_processing_complete_event';

  /**
   * The number of items in queue at the beginning of queue processing.
   *
   * @var int
   */

  protected $itemsInQueueStart = NULL;
  /**
   * The number of items released back to queue at the end of queue processing.
   *
   * @var int
   */
  protected $itemsInQueueFinish = NULL;

  /**
   * PostApiQueueFinishedEvent constructor.
   *
   * @param int $itemsInQueueStart
   *   The number of items in queue at the beginning of queue processing.
   * @param int $itemsInQueueFinish
   *   The number of items left in queue at the end of queue processing.
   */
  public function __construct(int $itemsInQueueStart, int $itemsInQueueFinish) {
    $this->itemsInQueueStart = $itemsInQueueStart;
    $this->itemsInQueueFinish = $itemsInQueueFinish;
  }

  /**
   * Returns the initial number of items in Queue.
   *
   * @return int|null
   *   The number of items in queue at the beginning of queue worker process.
   */
  public function getItemsInQueueStart() {
    return $this->itemsInQueueStart;
  }

  /**
   * Returns the final number of items in Queue after processing.
   *
   * @return int|null
   *   The number of items in queue at the end of queue worker process.
   */
  public function getItemsInQueueFinish() {
    return $this->itemsInQueueFinish;
  }

}
