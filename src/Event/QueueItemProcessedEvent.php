<?php

namespace Drupal\post_api\Event;

use Psr\Http\Message\ResponseInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the "Queue Item Processed" event.
 */
class QueueItemProcessedEvent extends Event {

  const EVENT_NAME = 'post_api_queue_item_processed_event';

  /**
   * The HTTP response received from POST request.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  public $response;

  /**
   * The queue item data.
   *
   * @var array
   */
  protected $queueItemData;

  /**
   * QueueItemProcessedEvent constructor.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response code received from POST request.
   * @param array $queueItemData
   *   The queue item data.
   */
  public function __construct(ResponseInterface $response, array $queueItemData) {
    $this->response = $response;
    $this->queueItemData = $queueItemData;
  }

  /**
   * Returns the HTTP response code.
   *
   * @return int
   *   The HTTP response code.
   */
  public function getResponseCode() {
    return $this->response->getStatusCode();
  }

  /**
   * Returns the HTTP reason phrase from the repsones.
   *
   * @return string
   *   The HTTP response reason phrase.
   */
  public function getReasonPhrase() {
    return $this->response->getReasonPhrase();
  }

  /**
   * Get the size from the response.
   *
   * @return float
   *   The size pulled from the response. 0 if NULL or FALSE.
   */
  public function getResponseSize():float {
    $length = $this->response->getHeader('Content-Length');
    $length = reset($length);
    $size = floatval($length);
    return $size;
  }

  /**
   * Returns the queue item data.
   *
   * @return array
   *   The queue item data.
   */
  public function getQueueItem() {
    return $this->queueItemData;
  }

}
